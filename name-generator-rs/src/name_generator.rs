extern crate rand;

use rand::Rng;

pub struct NameList {
    probablity: f32,
    affix: (&'static str, &'static str),
    list: Vec<&'static str>,
}

impl NameList {
    pub fn new(probablity: f32, affix: (&'static str, &'static str)) -> Self {
        NameList {
            probablity,
            affix,
            list: Vec::new(),
        }
    }

    pub fn default() -> Self {
        NameList {
            probablity: 1.0,
            affix: ("", ""),
            list: Vec::new(),
        }
    }

    pub fn from(
        probablity: f32,
        affix: (&'static str, &'static str),
        list: Vec<&'static str>,
    ) -> Self {
        NameList {
            probablity,
            affix,
            list,
        }
    }

    fn add(&mut self, new_element: &'static str) -> &Self {
        self.list.push(new_element);
        self
    }

    fn get_probabilty(&self) -> f32 {
        self.probablity
    }

    fn len(&self) -> usize {
        self.list.len()
    }

    fn at(&self, index: usize) -> &str {
        &self.list[index]
    }

    fn prefix(&self) -> &str {
        self.affix.0
    }

    fn postfix(&self) -> &str {
        self.affix.1
    }
}

fn generate_random_string(name_lists: &Vec<NameList>) -> String {
    let mut rng = rand::thread_rng();

    // Generate random indices
    let indices = {
        let mut indices = Vec::new();
        for list in name_lists {
            let list_probability = list.get_probabilty();
            if (list_probability != 0.0 && rng.gen_range(0.0, 1.0) >= 1.0 - list_probability)
                || list_probability == 1.0
            {
                let random_index = rng.gen_range(0usize, list.len());
                indices.push(random_index);
            }
        }
        indices
    };

    // Construct string from random indices
    let mut string_builder = String::new();
    for i in 0..indices.len() {
        let list = &name_lists[i];
        let random_index = indices[i];
        string_builder.push_str(&list.prefix());
        string_builder.push_str(&list.at(random_index));
        string_builder.push_str(&list.postfix());
    }
    string_builder
}

pub fn generate_permuation_list(count: usize, name_lists: &Vec<NameList>) -> Vec<String> {
    let mut return_strings = Vec::new();

    for _ in 0..count {
        return_strings.push(generate_random_string(name_lists));
    }

    return_strings
}

pub fn generate_permuation_list_unique(
    count: usize,
    max_retries: u8,
    name_lists: &Vec<NameList>,
) -> Vec<String> {
    let mut return_strings = Vec::new();
    let mut retry_count = 0;

    while return_strings.len() < count {
        let random_string = generate_random_string(name_lists);

        match return_strings.contains(&random_string) {
            true => {
                retry_count += 1;

                if retry_count >= max_retries {
                    return return_strings;
                }
            }
            false => {
                retry_count = 0;
                return_strings.push(random_string)
            }
        }
    }

    return_strings
}

#[test]
fn test_list_single() {
    let name_lists = vec![NameList::from(1.0, ("", ""), vec!["baron", "duke", "lord"])];
    let permuatations = generate_permuation_list(10, &name_lists);

    let expected = vec!["baron", "duke", "lord"];

    assert_eq!(permuatations.len(), 10);
    for permutation in &permuatations {
        let s: &str = permutation;
        assert!(expected.contains(&&s));
    }
}

#[test]
fn test_list_multi() {
    let name_lists = vec![
        NameList::from(1.0, ("", " "), vec!["baron", "duke", "lord"]),
        NameList::from(1.0, ("", ""), vec!["snakeworth", "grassman", "bob"]),
        NameList::from(0.5, (" the ", "!"), vec!["i", "viii"]),
    ];

    let permuatations = generate_permuation_list(50, &name_lists);

    let expected = vec![
        "baron snakeworth",
        "duke snakeworth",
        "lord snakeworth",
        "baron grassman",
        "duke grassman",
        "lord grassman",
        "baron bob",
        "duke bob",
        "lord bob",
        "baron snakeworth the i!",
        "duke snakeworth the i!",
        "lord snakeworth the i!",
        "baron grassman the i!",
        "duke grassman the i!",
        "lord grassman the i!",
        "baron bob the i!",
        "duke bob the i!",
        "lord bob the i!",
        "baron snakeworth the viii!",
        "duke snakeworth the viii!",
        "lord snakeworth the viii!",
        "baron grassman the viii!",
        "duke grassman the viii!",
        "lord grassman the viii!",
        "baron bob the viii!",
        "duke bob the viii!",
        "lord bob the viii!",
    ];

    assert_eq!(permuatations.len(), 50);
    for permutation in &permuatations {
        let s: &str = permutation;
        assert!(expected.contains(&&s));
    }
}

#[test]
fn test_list_multi_unique() {
    let name_lists = vec![
        NameList::from(1.0, ("", " "), vec!["baron", "duke", "lord"]),
        NameList::from(1.0, ("", ""), vec!["snakeworth", "grassman", "bob"]),
        NameList::from(0.5, (" the ", "!"), vec!["i", "viii"]),
    ];

    let permuatations = generate_permuation_list_unique(15, 10, &name_lists);

    let expected = vec![
        "baron snakeworth",
        "duke snakeworth",
        "lord snakeworth",
        "baron grassman",
        "duke grassman",
        "lord grassman",
        "baron bob",
        "duke bob",
        "lord bob",
        "baron snakeworth the i!",
        "duke snakeworth the i!",
        "lord snakeworth the i!",
        "baron grassman the i!",
        "duke grassman the i!",
        "lord grassman the i!",
        "baron bob the i!",
        "duke bob the i!",
        "lord bob the i!",
        "baron snakeworth the viii!",
        "duke snakeworth the viii!",
        "lord snakeworth the viii!",
        "baron grassman the viii!",
        "duke grassman the viii!",
        "lord grassman the viii!",
        "baron bob the viii!",
        "duke bob the viii!",
        "lord bob the viii!",
    ];

    let mut used = Vec::new();
    assert_eq!(permuatations.len(), 15);
    for permutation in &permuatations {
        let s: &str = permutation;
        assert!(expected.contains(&&s));
        assert!(!used.contains(&permutation));
        used.push(permutation);
    }
}
