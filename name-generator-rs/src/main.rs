//extern crate clap;
//use clap::{App, Arg, SubCommand};

mod name_generator;

fn star_names() -> Vec<name_generator::NameList> {
    let description = name_generator::NameList::from(
        1.0,
        ("", " "),
        vec![
            "Chris's",
            "Marco's",
            "Deep",
            "Shallow",
            "Turbid",
            "Titan",
            "Calving",
            "Birthing",
            "Computing",
            "Rising",
            "Falling",
            "Turning",
            "Massive",
            "Subducting",
            "Long",
            "Short",
            "Middle",
            "Waxing",
            "Neap",
            "Druid",
            "Warrior",
            "Archer",
            "Rogue",
            "Red",
            "Green",
            "Blue",
            "Orange",
            "Black",
            "White",
            "Dancing",
            "Ruptured",
            "Exploding",
            "Friendship",
            "Mega",
            "Terra",
            "Half",
            "Full",
            "Bright",
            "Dim",
            "Dark",
            "Light",
            "Dense",
            "Kinetic",
            "Dead",
            "Living",
            "Compressing",
            "Converging",
            "Diverging",
            "Viscous",
            "Melting",
            "Freezing",
            "Vaporizing",
            "Maximum",
            "Minimum",
            "Snake",
            "Main",
            "Sub",
            "Twisting",
            "Jolly",
        ],
    );
    let geography = name_generator::NameList::from(
        0.75,
        ("", ""),
        vec![
            "Corsair",
            "Bluff",
            "Hill",
            "Expanse",
            "Drift",
            "Star",
            "Extent",
            "Reach",
            "Legacy",
            "Fall",
            "Demise",
            "Diminish",
            "System",
            "Stand",
            "Fortress",
            "Castle",
            "Fortitude",
            "Hole",
            "Cluster",
            "Slide",
            "Flow",
            "Flood",
            "Debris",
            "Tectonics",
            "Belt",
            "Chain",
            "Ridge",
            "Sea",
            "Well",
            "Sink",
            "Swell",
            "Waves",
            "Period",
            "Spring",
            "Summer",
            "Winter",
            "Autumn",
            "Enum",
            "Class",
            "Lambda",
            "Function",
            "Sword",
            "Spear",
            "Arrow",
            "Shield",
            "Helm",
            "Bay",
            "Seal",
            "Law",
            "Rim",
            "Wall",
            "Matrix",
            "Nimbus",
            "Eruption",
            "Event",
            "Horizon",
            "Sunset",
            "Sunrise",
            "Moonrise",
            "Moonset",
            "Bell",
            "Gimbal",
            "Quater",
            "Mill",
            "Burst",
            "Mixture",
            "Phase",
            "Motion",
            "Machines",
            "Layer",
            "Number",
            "Infinity",
            "Boundary",
            "Layer",
            "Wind",
            "Path",
            "Shadow",
            "Penumbra",
            "Colours",
            "Time",
            "Atoll",
            "Retreat",
            "Core",
            "Profit",
            "Power",
            "Warmup",
            "Magnum",
            "Eagle",
            "Fox",
            "Shale",
            "Lizard",
            "Axe",
            "Jets",
        ],
    );
    let identifier = name_generator::NameList::from(
        0.25,
        (" ", ""),
        vec![
            "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "Alpha", "Beta", "Gamma",
            "Delta", "Epsilon", "Zeta", "Eta", "Theta", "Iota", "Kappa", "Mu", "Nu", "Xi", "Pi",
            "Rho", "Tau", "Upsilon", "Phi", "Chi", "Psi", "Omega", "Major", "Minor", "Super",
            "North", "South", "East", "West", "X", "Y", "Z", "Zero",
        ],
    );

    let body = name_generator::NameList::from(
        1.0,
        ("", " "),
        vec![
            "Star",
            "Galaxy",
            "Nebular",
            "Black hole",
            "Neutron Star",
            "Quasar",
            "Planet",
            "Gas Giant",
            "Astroid Belt",
            "Moon",
        ],
    );

    vec![body, description, geography, identifier]
}

fn captains_log() -> Vec<name_generator::NameList> {
    let intro = name_generator::NameList::from(1.0, ("", ""), vec!["captain's log, star date"]);
    let star_date = name_generator::NameList::from(
        1.0,
        (" [", "] - "),
        vec!["0xF345", "0xAF20", "0x83C5", "0xD220"],
    );
    let prose = name_generator::NameList::from(
        1.0,
        ("", " "),
        vec![
            "Today we make headway for the",
            "We set sail for",
            "From my port window the shadow of",
            "We maneuver past",
            "Our sensors are now picking up",
        ],
    );

    vec![intro, star_date, prose]
}

fn final_words() -> Vec<name_generator::NameList> {
    let status = name_generator::NameList::from(
        1.0,
        (". ", " "),
        vec![
            "We are low on",
            "The crew tell me its the holiday of",
            "I hear they have good",
            "Reports of rogues in sector",
            "Scouters indicate high level of",
            "I can see the hills of",
            "I can see the oceans of",
        ],
    );

    let jargon_a = name_generator::NameList::from(
        1.0,
        ("", ""),
        vec!["Tan", "Shan", "Shang", "Sh", "Th", "Wu", "Ah", "Bo", "Tek"],
    );

    let jargon = vec![
        "tan", "shan", "shang", "ou", "u", "bo", "a", "up", "eh", "i", "uk", "uf", "it", "bus",
    ];

    let jargon_b = name_generator::NameList::from(
        0.8,
        ("", ""),
        vec![
            "tan", "shan", "shang", "ou", "u", "bo", "a", "up", "eh", "i", "uk", "uf", "it", "bus",
        ],
    );

    let jargon_c = name_generator::NameList::from(
        0.25,
        ("", ""),
        vec![
            "tan", "shan", "shang", "ou", "u", "bo", "a", "up", "eh", "i", "uk", "uf", "it",
        ],
    );

    let jargon_d = name_generator::NameList::from(0.8, ("", ""), vec!["ed", "ing", "er", "s"]);

    let sign_off = name_generator::NameList::from(
        0.75,
        (". ", ""),
        vec![
            "I am filled with dread",
            "I am sure we can make it",
            "My time will come",
            "We are doomed",
        ],
    );
    vec![status, jargon_a, jargon_b, jargon_c, jargon_d, sign_off]
}

fn main() {
    let count = 2000;
    let star_names = name_generator::generate_permuation_list_unique(count, 100, &star_names());
    let captains_logg = name_generator::generate_permuation_list(count, &captains_log());
    let captains_sign_off = name_generator::generate_permuation_list(count, &final_words());

    for i in 0..count {
        println!(
            "> {}. {}{}{}",
            i, captains_logg[i], star_names[i], captains_sign_off[i]
        );
    }
}
